﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using CloudDemoTemplate.ViewModel;

namespace CloudDemoTemplate
{
    class CloudConnect
    {
		#region 【定義部】フィールド

		#region 【定義部】const
		/// <summary>
		/// TeraTerm実行ファイル名デフォルト値
		/// </summary>
		private const string conTeraTermExeName = "ttermpro.exe";
		#endregion 【定義部】const

		#region 【定義部】private
		/// <summary>
		/// ビューモデル
		/// </summary>
		private WindowCtrlVM clsWindowCtrlVM;
		/// <summary>
		/// TeraTerm実行ファイルのパス
		/// </summary>
		/// <remarks>パスのみ</remarks>
		private string strTeraTermExePath = "";
		/// <summary>
		/// TeraTerm実行ファイル名
		/// </summary>
		/// <remarks>ファイル名のみ</remarks>
		private string strTereTermExeName = "";
		/// <summary>
		/// エラーメッセージ
		/// </summary>
		private StringBuilder sbErrorMessage = new StringBuilder();
		/// <summary>
		/// IPアドレス正規表現
		/// </summary>
		private Regex regIPAddress = new Regex(@"/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/", RegexOptions.Compiled);
		#endregion 【定義部】private

		#endregion 【定義部】フィールド

		#region 【定義部】コンストラクタ
		/// <summary>
		/// コンストラクタ
		/// </summary>
		public CloudConnect(WindowCtrlVM clsWindowCtrlVM)
		{
			this.clsWindowCtrlVM = clsWindowCtrlVM;
		}
		#endregion 【定義部】コンストラクタ

		#region 【定義部】プロパティ
		/// <summary>
		/// エラーメッセージ
		/// </summary>
		public string ErrorMessage
		{
			get { return this.sbErrorMessage.ToString(); }
		}
		#endregion 【定義部】プロパティ

		#region 【定義部】イベント
		#endregion 【定義部】イベント

		#region 【定義部】メソッド

		#region 【定義部】public
		/// <summary>
		/// 接続関連ファイル出力処理
		/// </summary>
		/// <returns>true:成功/false:失敗</returns>
		/// <remarks>
		/// 接続関連ファイルは以下の3つ
		/// 　<接続文字列>.ttl、<接続文字列>.bat、TERATERM_<接続文字列>.INI
		/// </remarks>
		public bool FileOut()
		{
			//エラークリア
			this.sbErrorMessage.Clear();
			this.clsWindowCtrlVM.Message = "";

			//入力チェック
			if (!this._pInputCheck()) return false;

			//batファイル出力
			if (!this._pBatFileOut()) return false;

			//ttlファイル出力
			if (!this._pTtlFileOut()) return false;

			//INIファイル出力
			if (!this._pIniFileOut()) return false;

			return true;
		}
		/// <summary>
		/// 接続処理
		/// </summary>
		/// <remarks>ここではbatファイルを実行するのみです</remarks>
		public void Connect()
		{
			//エラークリア
			this.sbErrorMessage.Clear();
			this.clsWindowCtrlVM.Message = "";

			//バッチファイル実行
			this._pExecBatch();
		}
		#endregion 【定義部】public

		#region 【定義部】private
		/// <summary>
		/// 入力チェック
		/// </summary>
		/// <returns>true:エラーなし/false:エラーあり</returns>
		private bool _pInputCheck()
		{
			bool blnInputCheck = true;

			//TeraTerm実行ファイルの存在確認
			if (this.clsWindowCtrlVM.TeraTermExePath.ToLower().EndsWith(".exe"))
			{
				this.strTeraTermExePath = Path.GetDirectoryName(this.clsWindowCtrlVM.TeraTermExePath);
				this.strTereTermExeName = Path.GetFileName(this.clsWindowCtrlVM.TeraTermExePath);
			}
			else
			{
				this.strTeraTermExePath = this.clsWindowCtrlVM.TeraTermExePath;
				this.strTereTermExeName = conTeraTermExeName;
			}
			if (!this.strTeraTermExePath.EndsWith(@"\")) this.strTeraTermExePath += @"\";
			if (!Directory.Exists(this.strTeraTermExePath))
			{
				this.sbErrorMessage.AppendLine("TeraTerm実行ファイルのパスが存在しません。");
				blnInputCheck = false;
			}
			else if (!File.Exists(this.strTeraTermExePath + this.strTereTermExeName))
			{
				this.sbErrorMessage.AppendLine("TeraTerm実行ファイルが存在しません。");
				blnInputCheck = false;
			}

			//入力チェック
			if (this.clsWindowCtrlVM.ServerIPAddress.Trim() == "")
			{
				this.sbErrorMessage.AppendLine("サーバIPアドレスが入力されていません。");
				blnInputCheck = false;
			}
			if (this.clsWindowCtrlVM.ServerPort.Trim() == "")
			{
				this.sbErrorMessage.AppendLine("サーバポート番号が入力されていません。");
				blnInputCheck = false;
			}
			if (this.clsWindowCtrlVM.User.Trim() == "")
			{
				this.sbErrorMessage.AppendLine("接続文字列(SSHのユーザ名)が入力されていません。");
				blnInputCheck = false;
			}
			if (this.clsWindowCtrlVM.KeyFilePath.Trim() == "")
			{
				this.sbErrorMessage.AppendLine("秘密鍵パスが入力されていません。");
				blnInputCheck = false;
			}
			if (this.clsWindowCtrlVM.LocalPort.Trim() == "")
			{
				this.sbErrorMessage.AppendLine("ローカルのポートが入力されていません。");
				blnInputCheck = false;
			}
			if (this.clsWindowCtrlVM.RemoteHost.Trim() == "")
			{
				this.sbErrorMessage.AppendLine("リモート側ホストが入力されていません。");
				blnInputCheck = false;
			}
			if (this.clsWindowCtrlVM.RemotePort.Trim() == "")
			{
				this.sbErrorMessage.AppendLine("リモート側ポートが入力されていません。");
				blnInputCheck = false;
			}

			this.clsWindowCtrlVM.Message = this.sbErrorMessage.ToString();

			return blnInputCheck;
		}

		/// <summary>
		/// batファイル出力処理
		/// </summary>
		/// <returns>true:成功/false:失敗</returns>
		/// <remarks>
		/// テンプレートファイル(CloudDemoTemplate.bat)を開いて内容を置換後、
		/// <接続文字列>.batファイル名で出力する
		/// </remarks>
		private bool _pBatFileOut()
		{
			//埋め込みファイルの読み込み
			List<string> lstLines = new List<string>();
			Assembly assembly = Assembly.GetExecutingAssembly();
			using (Stream stream = assembly.GetManifestResourceStream("CloudDemoTemplate.Template.CloudDemoTemplate.bat"))
			{
				using (StreamReader streamReader = new StreamReader(stream))
				{
					string strLine = "";
					while ((strLine = streamReader.ReadLine()) != null)
					{
						//置換してリストに追加
						strLine = strLine.Replace("<TeraTermExePath>", this.strTeraTermExePath + this.strTereTermExeName);
						strLine = strLine.Replace("<User>", this.clsWindowCtrlVM.User);
						strLine = strLine.Replace("<LocalPort>", this.clsWindowCtrlVM.LocalPort);
						lstLines.Add(strLine);
					}
				}
			}

			//ファイル出力
			string strOutputFile = @".\" + this.clsWindowCtrlVM.User + ".bat";
			using (StreamWriter streamWriter = new StreamWriter(strOutputFile, false, Encoding.GetEncoding("shift_jis")))
			{
				foreach (string strLine in lstLines)
				{
					streamWriter.WriteLine(strLine);
				}
			}

			return true;
		}

		/// <summary>
		/// ttlファイル出力処理
		/// </summary>
		/// <returns>true:成功/false:失敗</returns>
		/// <remarks>
		/// テンプレートファイル(CloudDemoTemplate.ttl)を開いて内容を置換後、
		/// <接続文字列>.ttlファイル名で出力する
		/// </remarks>
		private bool _pTtlFileOut()
		{
			//埋め込みファイルの読み込み
			List<string> lstLines = new List<string>();
			Assembly assembly = Assembly.GetExecutingAssembly();
			using (Stream stream = assembly.GetManifestResourceStream("CloudDemoTemplate.Template.CloudDemoTemplate.ttl"))
			{
				using (StreamReader streamReader = new StreamReader(stream))
				{
					string strLine = "";
					while ((strLine = streamReader.ReadLine()) != null)
					{
						//置換してリストに追加
						strLine = strLine.Replace("<ServerIPAddress>", this.clsWindowCtrlVM.ServerIPAddress);
						strLine = strLine.Replace("<ServerPort>", this.clsWindowCtrlVM.ServerPort);
						strLine = strLine.Replace("<User>", this.clsWindowCtrlVM.User);
						strLine = strLine.Replace("<KeyFilePath>", this.clsWindowCtrlVM.KeyFilePath);
						lstLines.Add(strLine);
					}
				}
			}

			//ファイル出力
			string strOutputFile = this.strTeraTermExePath + this.clsWindowCtrlVM.User + ".ttl";
			using (StreamWriter streamWriter = new StreamWriter(strOutputFile, false, Encoding.GetEncoding("shift_jis")))
			{
				foreach (string strLine in lstLines)
				{
					streamWriter.WriteLine(strLine);
				}
			}

			return true;
		}

		/// <summary>
		/// INIファイル出力処理
		/// </summary>
		/// <returns>true:成功/false:失敗</returns>
		/// <remarks>
		/// テンプレートファイル(TERATERM_CloudDemoTemplate.INI)を開いて内容を置換後、
		/// TERATERM_<接続文字列>.INIファイル名で出力する
		/// </remarks>
		private bool _pIniFileOut()
		{
			//埋め込みファイルの読み込み
			List<string> lstLines = new List<string>();
			Assembly assembly = Assembly.GetExecutingAssembly();
			using (Stream stream = assembly.GetManifestResourceStream("CloudDemoTemplate.Template.TERATERM_CloudDemoTemplate.INI"))
			{
				using (StreamReader streamReader = new StreamReader(stream))
				{
					string strLine = "";
					while ((strLine = streamReader.ReadLine()) != null)
					{
						//置換(DefaultForwarding=の行のみ)してリストに追加
						if (strLine.StartsWith("DefaultForwarding="))
						{
							strLine = strLine.Replace("<LocalPort>", this.clsWindowCtrlVM.LocalPort);
							strLine = strLine.Replace("<RemoteHost>", this.clsWindowCtrlVM.RemoteHost);
							strLine = strLine.Replace("<RemotePort>", this.clsWindowCtrlVM.RemotePort);
						}
						lstLines.Add(strLine);
					}
				}
			}

			//ファイル出力
			string strOutputFile = this.strTeraTermExePath + "TERATERM_" + this.clsWindowCtrlVM.User + ".INI";
			using (StreamWriter streamWriter = new StreamWriter(strOutputFile, false, Encoding.GetEncoding("shift_jis")))
			{
				foreach (string strLine in lstLines)
				{
					streamWriter.WriteLine(strLine);
				}
			}

			return true;
		}

		/// <summary>
		/// バッチファイル実行処理
		/// </summary>
		private void _pExecBatch()
		{
			string strBatchName = @".\" + this.clsWindowCtrlVM.User + ".bat";
			ProcessStartInfo psiBatch = new ProcessStartInfo();
			psiBatch.FileName = System.Environment.GetEnvironmentVariable("ComSpec");
			psiBatch.CreateNoWindow = true;
			psiBatch.UseShellExecute = false;
			psiBatch.Arguments = @"/c " + strBatchName;
			Process prcBatch = Process.Start(psiBatch);
			prcBatch.WaitForExit();
		}
		#endregion 【定義部】private

		#endregion 【定義部】メソッド
	}
}
