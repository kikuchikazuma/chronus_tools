﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace CloudDemoTemplate.ViewModel
{
    /// <summary>
    /// ベースＶＭ
    /// </summary>
    public class BaseVM : INotifyPropertyChanged
    {
        #region プロパティチェンジ

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
                PropertyChangedEventHandler handler = this.PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
