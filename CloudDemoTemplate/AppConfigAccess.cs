﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudDemoTemplate.Model;

namespace CloudDemoTemplate
{
    class AppConfigAccess
    {
		#region 【定義部】メソッド

		#region 【定義部】public
		public static AppConfigDataM LoadData()
		{
			AppConfigDataM ctlRetData = new AppConfigDataM();
			try
			{
				ctlRetData.TeraTermExePath = Properties.Settings.Default.TeraTermExePath;
				ctlRetData.ServerIPAddress = Properties.Settings.Default.ServerIPAddress;
				ctlRetData.ServerPort = Properties.Settings.Default.ServerPort;
				ctlRetData.User = Properties.Settings.Default.User;
				ctlRetData.KeyFilePath = Properties.Settings.Default.KeyFilePath;
				ctlRetData.LocalPort = Properties.Settings.Default.LocalPort;
				ctlRetData.RemoteHost = Properties.Settings.Default.RemoteHost;
				ctlRetData.RemotePort = Properties.Settings.Default.RemotePort;
			}
			catch (EntryPointNotFoundException)
			{
			}
			catch(Exception ex)
			{
				System.Windows.MessageBox.Show(ex.Message);
			}

			return ctlRetData;
		}

		public static void SaveData(AppConfigDataM ctlData)
		{
			Properties.Settings.Default.TeraTermExePath = ctlData.TeraTermExePath;
			Properties.Settings.Default.ServerIPAddress = ctlData.ServerIPAddress;
			Properties.Settings.Default.ServerPort = ctlData.ServerPort;
			Properties.Settings.Default.User = ctlData.User;
			Properties.Settings.Default.KeyFilePath = ctlData.KeyFilePath;
			Properties.Settings.Default.LocalPort = ctlData.LocalPort;
			Properties.Settings.Default.RemoteHost = ctlData.RemoteHost;
			Properties.Settings.Default.RemotePort = ctlData.RemotePort;
			Properties.Settings.Default.Save();
		}
		#endregion 【定義部】public

		#region 【定義部】private
		#endregion 【定義部】private

		#endregion 【定義部】メソッド
	}
}
