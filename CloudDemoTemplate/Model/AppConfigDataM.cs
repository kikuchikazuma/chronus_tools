﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudDemoTemplate.Model
{
    public class AppConfigDataM
    {
		#region 【定義部】フィールド

		#region 【定義部】const
		public const string conLocalPortDefault = "13389";
		public const string conRemotePortDefault = "3389";
		#endregion 【定義部】const

		#region 【定義部】private
		string strTeraTermExePath = "";
		string strServerIPAddress = "";
		string strServerPort = "";
		string strUser = "";
		string strKeyFilePath = "";
		string strLocalPort = "";
		string strRemoteHost = "";
		string strRemotePort = "";
		#endregion 【定義部】private

		#endregion 【定義部】フィールド

		#region 【定義部】プロパティ
		public string TeraTermExePath
		{
			get { return this.strTeraTermExePath; }
			set { this.strTeraTermExePath = value; }
		}
		public string ServerIPAddress
		{
			get { return this.strServerIPAddress; }
			set { this.strServerIPAddress = value; }
		}
		public string ServerPort
		{
			get { return this.strServerPort; }
			set { this.strServerPort = value; }
		}
		public string User
		{
			get { return this.strUser; }
			set { this.strUser = value; }
		}
		public string KeyFilePath
		{
			get { return this.strKeyFilePath; }
			set { this.strKeyFilePath = value; }
		}
		public string LocalPort
		{
			get { return this.strLocalPort; }
			set { this.strLocalPort = value; }
		}
		public string RemoteHost
		{
			get { return this.strRemoteHost; }
			set { this.strRemoteHost = value; }
		}
		public string RemotePort
		{
			get { return this.strRemotePort; }
			set { this.strRemotePort = value; }
		}
		#endregion 【定義部】プロパティ
	}
}
