﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace CloudDemoTemplate
{
	/// <summary>
	/// MainWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class MainWindow : Window
	{
		#region 【定義部】フィールド

		#region 【定義部】private
		private ViewModel.WindowCtrlVM clsWindowCtrlVM;
		#endregion 【定義部】private

		#endregion 【定義部】フィールド

		#region 【定義部】コンストラクタ
		/// <summary>
		/// コンストラクタ
		/// </summary>
		public MainWindow()
		{
			InitializeComponent();

			//app.configの読み込み
			this.clsWindowCtrlVM = new ViewModel.WindowCtrlVM();

			//バインド
			this._pBindControl();
		}
		#endregion 【定義部】コンストラクタ

		#region 【定義部】イベント
		/// <summary>
		/// TeraTerm実行ファイルの参照ボタン_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnTeraTermExePath_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog dlgRef = new OpenFileDialog();

			string strDirectoryName = "";
			string strFileName = "";
			this._pDivideDirectoryAndFile(this.txtTeraTermExePath.Text, out strDirectoryName, out strFileName);
			if (strDirectoryName == "") strDirectoryName = @"C:\";
			dlgRef.InitialDirectory = strDirectoryName;
			dlgRef.Title = "TeraTerm実行ファイルを選んで下さい";
			dlgRef.Filter = "実行ファイル|*.exe|全てのファイル|*.*";
			if (dlgRef.ShowDialog() == true)
			{
				this.txtTeraTermExePath.Text = dlgRef.FileName;
			}
		}

		/// <summary>
		/// 秘密鍵パスの参照ボタン_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnKeyFilePath_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog dlgRef = new OpenFileDialog();

			string strDirectoryName = "";
			string strFileName = "";
			this._pDivideDirectoryAndFile(this.txtKeyFilePath.Text, out strDirectoryName, out strFileName);
			if (strDirectoryName == "") strDirectoryName = @"C:\";
			dlgRef.InitialDirectory = strDirectoryName;
			dlgRef.Title = "秘密鍵のファイルを選んで下さい";
			dlgRef.Filter = "全てのファイル|*.*";
			if (dlgRef.ShowDialog() == true)
			{
				this.txtKeyFilePath.Text = dlgRef.FileName;
			}
		}

		/// <summary>
		/// 接続ボタン_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnConnect_Click(object sender, RoutedEventArgs e)
		{
			CloudConnect clsCloudConnect = new CloudConnect(this.clsWindowCtrlVM);

			//接続関連ファイル出力
			if (!clsCloudConnect.FileOut()) return;

			//接続実行
			clsCloudConnect.Connect();

			//app.configへの書き込み
			AppConfigAccess.SaveData(this.clsWindowCtrlVM.AppConfigDataM);
		}

		/// <summary>
		/// 終了ボタン_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnClose_Click(object sender, RoutedEventArgs e)
		{
			//アプリ終了
			this.Close();
		}
		#endregion 【定義部】イベント

		#region 【定義部】メソッド

		#region 【定義部】public
		#endregion 【定義部】public

		#region 【定義部】private
		/// <summary>
		/// バインド処理
		/// </summary>
		private void _pBindControl()
		{
			this.pnlInputArea.DataContext = this.clsWindowCtrlVM;
		}
		/// <summary>
		/// ディレクトリーとファイル名の分割
		/// </summary>
		/// <param name="strPath"></param>
		/// <param name="strDirectoryName"></param>
		/// <param name="strFileName"></param>
		private void _pDivideDirectoryAndFile(string strPath, out string strDirectoryName, out string strFileName)
		{
			strDirectoryName = "";
			strFileName = "";
			if (System.IO.File.Exists(strPath))
			{
				strDirectoryName = System.IO.Path.GetDirectoryName(strPath);
				strFileName = System.IO.Path.GetFileName(strPath);
			}
			else if (System.IO.Directory.Exists(strPath))
			{
				strDirectoryName = strPath;
			}
		}
		#endregion 【定義部】private

		#endregion 【定義部】メソッド
	}
}
