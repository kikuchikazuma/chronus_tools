﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CloudDemoTemplate.Model;
using System.Collections.ObjectModel;

namespace CloudDemoTemplate.ViewModel
{
    /// <summary>
    /// ウィンドウ制御ビューモデル
    /// </summary>
    public class WindowCtrlVM : BaseVM
    {
		/// <summary>
		/// app.configモデル
		/// </summary>
		AppConfigDataM clsAppConfigDataM;
		/// <summary>
		/// メッセージ
		/// </summary>
		private string strMessage;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public WindowCtrlVM()
        {
			//app.configの読み込み
			this.clsAppConfigDataM = AppConfigAccess.LoadData();
		}

		#region プロパティ

		public Model.AppConfigDataM AppConfigDataM
		{
			get { return this.clsAppConfigDataM; }
		}

		public string TeraTermExePath
		{
			get { return this.clsAppConfigDataM.TeraTermExePath; }
			set
			{
				this.clsAppConfigDataM.TeraTermExePath = value;
				this.OnPropertyChanged("TeraTermExePath");
			}
		}
		public string ServerIPAddress
		{
			get { return this.clsAppConfigDataM.ServerIPAddress; }
			set
			{
				this.clsAppConfigDataM.ServerIPAddress = value;
				this.OnPropertyChanged("ServerIPAddress");
			}
		}
		public string ServerPort
		{
			get { return this.clsAppConfigDataM.ServerPort; }
			set
			{
				this.clsAppConfigDataM.ServerPort = value;
				this.OnPropertyChanged("ServerPort");
			}
		}
		public string User
		{
			get { return this.clsAppConfigDataM.User; }
			set
			{
				this.clsAppConfigDataM.User = value;
				this.OnPropertyChanged("User");
			}
		}
		public string KeyFilePath
		{
			get { return this.clsAppConfigDataM.KeyFilePath; }
			set
			{
				this.clsAppConfigDataM.KeyFilePath = value;
				this.OnPropertyChanged("KeyFilePath");
			}
		}
		public string LocalPort
		{
			get { return this.clsAppConfigDataM.LocalPort; }
			set
			{
				this.clsAppConfigDataM.LocalPort = value;
				this.OnPropertyChanged("LocalPort");
			}
		}
		public string RemoteHost
		{
			get { return this.clsAppConfigDataM.RemoteHost; }
			set
			{
				this.clsAppConfigDataM.RemoteHost = value;
				this.OnPropertyChanged("RemoteHost");
			}
		}
		public string RemotePort
		{
			get { return this.clsAppConfigDataM.RemotePort; }
			set
			{
				this.clsAppConfigDataM.RemotePort = value;
				this.OnPropertyChanged("RemotePort");
			}
		}
		public string Message
		{
			get { return this.strMessage; }
			set
			{
				this.strMessage = value;
				this.OnPropertyChanged("Message");
			}
		}

        #endregion

    }
}
